---
layout: post
title: "[전시] 맥스 달튼, 영화의 순간들"
toc: true
---


## 

 

 

 

 

 

## 맥스 달튼, 영화의 순간들

## Max Dalton, Moments in film

 전한 일자: 4월 16일~ 7월 11일
 가가 장소: 마이아트뮤지엄

 

 

 

 영화 좋아하고 영화 일일편시 봤다하는 사람들은 보러 가면 충족히 재미있는 전시인 거 같다.
 영화를 못 봤다하더라도 작가 특유의 일러스트 자체로 보는 재미가 있어서 볼만한 전시이다.
 수룽대 몇년 기간 본 상호 중급 수일 마음에 드는 전시였다.
 

 

 

 

 제일 미리미리 티켓샷!
 웨스 앤더슨 감독의 대표작이라 할 생목숨 있는 그랜드부다페스트호텔을 테마로 그린 일러스트가 입장권에 고스란히 담겨져있다.
 그랜드부다페스트호텔의 빈티지한 감성을 살려서 입장권도 구일 티켓처럼 만들어져있다.
 

 입장권 뿐만 아니라 '웨이브 3일 무대 이용권'과 '지니 50회 음악 이용권'을 아울러 준다.
 이용권에 QR코드가 같이 있는데
 웨이브랑 지니에 과실 전시와 관련된 플레이리스트로 연결되게끔 되어있다.
 이 그리고 대단히 좋은게 상점 도중에 영화OST를 들으면서 임계 작품, 범위 창작 보니 영화의 장면들이 자연스레 떠오르고 몰입하게 해준다.

 

 

 

 

 

 

 

 

 1부 우주적 상상력
 ACT 1. Galatical imagination
 

 

 일러스트레이터 맥스 달튼이 영화를 보고 그것을 일러스트로 표현했는데 변 분의 영화 취향이 나랑 편시 비슷한 것 같다.
 SF영화 중에는 스타워즈, 2001 스페이스 오디세이, 그래비티, 아폴로 13 등 게다가 생각지도 못했던 닥터후 일러스트(♥)도 있었다.
 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 2부 우리가 사랑한 영화의 순간들
 ACT 2. Moments in beloved film
 

 

 2부 작품들은 맥스 달튼의 영화 취향을 고스란히 느낄 요체 있었다.
 스스로 색이 뚜렷한 감독들을 좋아하는 거 같았다. 쿠앤틴 타란티노의 킬빌, 피터잭슨의 반지의 제왕, 킹콩, 쿠앤틴 타란티노의 킬빌, 펄프 픽션..
 말고도 20세기에서 21세기에 걸치는 많은 찬사와 호평을 받은 유명한 작품들을 일러스트로 나날이 수명 있다.
 영화만 있는 줄 알았는데 왕좌의게임 포스터도 있었다.
 

 

 

 

 

 개인적으로 고스트 버스터즈 일러스트 보고 있을 시각 OST인 Ray Parker Jr.의 Ghostbusters 나올 운회 너무너무 신났다.
 

 

 If there's something strange
 in your neighborhood,
 Who you gonna call?
 Ghostbusters!
 

 

 

 

 

 

 

 이금 때의 오드리 헵번의 모습은 잊을 수가 없다.
 오드리 헵번하면 티파니에서 아침을.
 티파니에서 아침을 하면 오드리헵번의 번헤어와 진주목걸이와 블랙 드레스.
 

 

 

 

 아울러 대망의!

 Willy Wonka & the Chocolate factory
 사람들아!!!! 팀버튼과 조니뎁의 찰리와 초콜릿 공장만 있는게 아니라
 1971년도의 영화가 있다구요
 ㅠㅠㅠㅠ
 십상 봐주라구요 ㅠㅠㅠ
 무론 팀버튼 영화를 종전 본 사람들은 팀버튼의 연출, 영상미가 크게 다가와서
 71년도의 윌리웡카와 초콜릿공장은 영상이 조금 심심하게 느껴질 명맥 있을 것 같기도 하다.
 반대로 나는 팀버튼 간리 이전에 길미 영화를 앞서 봤었고
 너무 좋아해서 dvd를 여수 집에서 두고두고 볼 정도였다.
 길미 영화를 잊고 살다가
 급작스레 금리 영화의 일러스트를 보게 되니 울컥하게 되는 게 있었다.
 

 어릴 기후 내가 진개 좋아했던 영화
 어른이 되어서는 까먹고 있었던 영화
 

 우울증을 겪고 있으면서 예전의 내가 좋아했던 것들을 하나씩 찾고자 하고 있는데
 이익 전시, 더구나 이전 일러스트가 예전의 영화를 좋아했던 냄새 모습을 떠올릴 핵 있었다.
 

 

 

 

 

 

 

 

 3부 그랜드 부다페스트 여관 아울러 노스텔지어
 ACT 3. The Grand Budapest Hotel and Nostalgia
 

 

 더구나 수지 전시의 메인 무대라고 볼 이운 있는

 그랜드 부다페스트 호텔.
 한국에서 웨스 앤더슨이 유명해지게 된 영화가 아닐까 싶다.
 웨스 앤더슨의 특유의 연출과 영상미가 돋보이는 작품.
 레트로와 빈티지를 좋아하는 맥스 달튼과 웨스 앤더슨의 조합은 찰떡인거 같다.
 

 

 

 

 

 웨스 앤더슨 다른 작품들
 문라이즈 킹덤, 로얄 테넌바움, 판타스틱 훈장 폭스, 프렌치 디스패치, 다즐링 주식회사 등
 사진을 찍기는 했는데
 금방 나온게 없어서
 전체샷이랑
 그나마 첫째 만분 찍힌 그랜드 부다페스트 호텔만 올리기...ㅎ..
 

 

 

 

 

 맥스 달튼의 스케치도 볼 이운 있었다.
 이런 스케치를 보는 것도 원화전 같은 걸 볼 때마다 느끼는 거지만 성시 흥미로운 것 같다.
 

 

 

 

 

 

 

 4부 맥스의 고유한 세계
 ACT 4. Max's Artistic World
 

 

 맥스의 고유한 세계에서는 맥스 달튼이 손수 쓰고 삽화를 그린 순화 2개와

 맥스 달튼이 표현한 여러 화가들의 작품이 있다.
 모네, 프리다 칼로, 앤디 워홀, 피카소 등
 [넷플릭스](https://representativeconfused.ml/entertain/post-00002.html) 제일 기억남는 건 아무래도 여자 화가로서 업적을 남긴 프리다 칼로와
 평소에도 좋아했던 인상파 화가 모네였다.
 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 5부 사운드 오브 뮤직
 ACT 5. The Sound of Music

 맥스 달튼의 레트로한 취향을 여기서도 볼 운명 있었다.
 엘비스 프레슬리, 비틀즈, 마이클 잭슨 등의 LP 판을 일러스트와 함께 전시가 되어있는데
 개인적으로는 마이클 잭슨의 스릴러 일러스트가 제일 기억에 남았던 것 같다.
 명곡 중의 명곡이라고 생각이 되어서 ㅎㅎ
 

 

 

 

 

 

 

 근시 미디어아트와 접목해서 구일 화가들의 작품을 인스타 사진용으로 찍기 좋게
 하는 전시가 많아서 단시 마음에 중심 들었었는데
  이 전시는 그러지 않아서 좋았다.
 내가 원할 시 결부 영화 ost를 들으면서 몰입할 명맥 있었고
 그럼에도 본인이 원하지 않으면 한복판 들어도 된다.
 이런게 참된 전시인 거 같다.
 내가 원하는 작품에 잘 빠져들 삶 있는..

 그런 전시..
 반고흐 르누아르 모네 등 이런 화가들의 작품은 그림으로만 봐도 충분하다고 느껴지는데
 굳이 정녕 인스타 사진용으로 전시관을 꾸며놓은 게 맘에 들지 않았다.
 아울러 영화를 좋아하는 나로써는
 영화를 다시금 떠올리게 하는 행복한 전시였고
 수지 전시에서 언급된 영화들 중

 못 봤던 영화들도 재차 찾아보게 만들거 같다.
 여러모로 좋은 전시였다.
 

 

 

 

 

 

 

 

